﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nordwesteckenregel
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test Kommentar um git zu testen
            Matrix matrix = new Matrix();
            matrix.MatrixAusgeben(matrix.einheitskostenmatrix);
            matrix.loesung = matrix.MatrixminimumLoesung(matrix.einheitskostenmatrix);
            matrix.loesung = matrix.SteppingStoneIteration(matrix.einheitskostenmatrix, matrix.loesung);
  //          matrix.MatrixAusgeben(matrix.loesung);
            Console.WriteLine("Kosten K = " + matrix.BerechneKosten(matrix.loesung,matrix.einheitskostenmatrix) + " Euro");
            Console.ReadLine();
            
        }

    }
}
