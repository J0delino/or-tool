﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nordwesteckenregel
{
    class Matrix
    {
        private int anzahlSpalten;
        private int anzahlZeilen;

        public void setAnzahlSpalten(int anzahl)
        {
            this.anzahlSpalten = anzahl;
        }
        public int getAnzahlSpalten()
        {
            return this.anzahlSpalten;
        }

        public void setAnzahlZeilen(int anzahl)
        {
            this.anzahlZeilen = anzahl;
        }
        public int getAnzahlZeilen()
        {
            return this.anzahlZeilen;
        }

        public int[,] einheitskostenmatrix;
        public int[,] loesung;

        /// <summary>
        /// Konstruktor, erstellt ein Matrix-Objekt.
        /// </summary>
        /// <param name="Zeilen">Anzahl der Zeilen der Einheitskostenmatrix, nur Daten, inkl. Bedarf-Zeile</param>
        /// <param name="Spalten">Anzahl der Salten der Einheitskostenmatrix, nur Daten, inkl. Nachfrage-Spalte</param>
        public Matrix(int Zeilen, int Spalten)
        {
            this.setAnzahlZeilen(Zeilen);
            this.setAnzahlSpalten(Spalten);
            this.einheitskostenmatrix = new int[Zeilen, Spalten];
        }
        /// <summary>
        /// Parameterloser Konstruktor, erstellt ein Matrix-Objekt. Eingabe der Daten erfolgt durch den Benutzer während des Programmablaufs.
        /// </summary>
        public Matrix()
        {
            this.einheitskostenmatrix = DatenEingeben();
            this.setAnzahlSpalten(this.einheitskostenmatrix.GetLength(0));
            this.setAnzahlZeilen(this.einheitskostenmatrix.GetLength(1));
        }
        /// <summary>
        /// Verlangt vom Benutzer alle nötigen Eingaben zur Erstellung der Einheitskostenmatrix und gibt diese als zweidimensionales Integer-Array zurück.
        /// </summary>
        /// <returns></returns>
        public int[,] DatenEingeben()
        {
            Console.WriteLine("Wieviele Spalten hat die Einheitskostenmatrix, inkl. der Nachfrage-Spalte?");
            int spalten;
            int zeilen;
            bool eingabetest = Int32.TryParse(Console.ReadLine(), out spalten);
            if (!eingabetest)
            {
                Console.WriteLine("Falsche Eingabe. Starten Sie das Programm neu und geben Sie nur Zahlen ein, verwenden Sie bei Kommazahlen einen Punkt als Trennzeichen und kein Komma.");
                Console.ReadLine();
                Environment.Exit(1);
            }

            Console.WriteLine("Wieviele Zeilen hat die Einheitskostenmatrix, inkl. der Bedarfs-Zeile?");
            eingabetest = Int32.TryParse(Console.ReadLine(), out zeilen);
            if (!eingabetest)
            {
                Console.WriteLine("Falsche Eingabe. Starten Sie das Programm neu und geben Sie nur Zahlen ein, verwenden Sie bei Kommazahlen einen Punkt als Trennzeichen und kein Komma.");
                Console.ReadLine();
                Environment.Exit(1);
            }
            int[,] matrix = new int[zeilen, spalten];

            Console.WriteLine("Geben Sie nun alle Zahlen inkl. Angebot und Bedarf zeilenweise nacheinander durch je ein Leerzeichen getrennt ein. Bei " + spalten + " Spalten und " + zeilen + " Zeilen ergeben sich damit " + spalten * zeilen + " Zahlenwerte, die Sie jetzt eingeben.");
            //beim Entwickeln zu faul die Zahlen jedes mal in der Konsole einzugeben
            string matrixzahlen = "10 16 13 1 160 4 11 8 4 190 7 2 6 9 40 40 200 60 90 390"; //Console.ReadLine();
            string[] matrixzahlenGetrennt = matrixzahlen.Split();
            int[] matrixzahlenInt = new int[matrixzahlenGetrennt.Length];

            //Alle Zahlen der Matrix werden von einem eindimensionalen String-Array in ein eindimensionales Int-Array konvertiert.
            for (int i = 0; i < matrixzahlenGetrennt.Length; i++)
            {
                eingabetest = Int32.TryParse(matrixzahlenGetrennt[i], out matrixzahlenInt[i]);
                if (!eingabetest)
                {
                    Console.WriteLine("Es gab ein Problem bei der " + i + ". Zahl. Starten Sie das Programm neu und achten Sie auf eine fehlerfreie Eingabe.");
                    Console.ReadLine();
                    Environment.Exit(2);
                }
            }

            //Das eindimensionale Int-Array wird in ein zweidimensionales Int-Array umgewandelt, um die Matrix richtig darzustellen.
            int elementCounter = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = matrixzahlenInt[elementCounter];
                    elementCounter++;
                }
            }

            return matrix;
        }
        /// <summary>
        /// Gibt eine beliebige übergebene Matrix schön formatiert aus.
        /// </summary>
        /// <param name="matrix">Matrix in Form eines zweidimensionalen int-Arrays, die ausgegeben werden soll.</param>
        public void MatrixAusgeben(int[,] matrix)
        {
            string platzhalter = "        ";
            //Erste Zeile der Tabelle
            string Headline = platzhalter + "  ";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                Headline += "B" + (i + 1) + platzhalter + " ";
            }
            Headline += "Angebot ai";
            Console.WriteLine(Headline);
            //bis hier

            //Tabellenrumpf wird in dieser Schleife abgehandelt
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                if (i == matrix.GetLength(0) - 1)
                {//Letzte Zeile der Tabelle fängt anders an
                    Console.Write("Bedarf bj ");
                }
                else
                {//Angebotszeilen fangen mit A1, A2,... an
                    Console.Write("A" + i + platzhalter);
                }

                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j].ToString("000") + platzhalter);
                }
                Console.Write("\n");
            }

        }
        /// <summary>
        /// Löst ein Transportproblem nach der Nordwesteckenregel und gibt eine entsprechende Lösungsmatrix in Form eines zweidimensionalen integer-Arrays zurück. Die zurückgegebene Matrix hat logischerweise die gleichen Ausmaße wie die übergebene Matrix.
        /// </summary>
        /// <param name="ekm">Einheitskostenmatrix mit Angebots- und Bedarfsinformationen in einer Matrix kombiniert dargestellt.</param>
        /// <returns></returns>
        public int[,] NordwesteckenLoesung(int[,] ekm) //ekm = Einheitskostenmatrix
        {
            int[,] Loesung = new int[ekm.GetLength(0), ekm.GetLength(1)];
            int i = 0;
            int j = 0;

            //Lösungsmatrix der Übersichtlichkeit halber mit Angebot und Bedarfsinformationen versehen
            for (int ii = 0; ii < Loesung.GetLength(1); ii++) //Bedarfszeile, so oft wie Spalten vorhanden sind, deshlb GetLength(1), 1=Spalten
            {
                Loesung[Loesung.GetLength(0) - 1, ii] = ekm[ekm.GetLength(0) - 1, ii];
            }
            for (int jj = 0; jj < Loesung.GetLength(0); jj++)
            {
                Loesung[jj, Loesung.GetLength(1) - 1] = ekm[jj, ekm.GetLength(1) - 1];
            }


            while (i < ekm.GetLength(1) - 1 || j < ekm.GetLength(0) - 1)
            {

                Console.WriteLine("");
                MatrixAusgeben(Loesung);
                Console.WriteLine("");
                //Console.ReadLine();

                if (ekm[i, ekm.GetLength(1) - 1] > ekm[ekm.GetLength(0) - 1, j]) //Angebot größer als Bedarf
                {
                    Loesung[i, j] = ekm[ekm.GetLength(0) - 1, j]; //Bedarf in Matrix schreiben, weil er vollständig gedeckt werden kann
                    Loesung[i, Loesung.GetLength(1) - 1] = ekm[i, ekm.GetLength(1) - 1] - Loesung[i, j];//Differenz für übriges Angebot ausrechnen
                    ekm[i, ekm.GetLength(1) - 1] = ekm[i, ekm.GetLength(1) - 1] - Loesung[i, j]; //Auch in ekm das neue Angebot schreiben
                    ekm[ekm.GetLength(0) - 1, j] = 0; //Übriger Bedarf auf Null setzen
                    Loesung[Loesung.GetLength(0) - 1, j] = 0; //Auch in Loesung auf Null setzen
                    j++; //Zeile bleibt, nach rechts in nächste Spalte rücken weil ja noch Angebot übrig ist.

                }
                else if (ekm[i, ekm.GetLength(1) - 1] < ekm[ekm.GetLength(0) - 1, j]) //Bedarf größer als Angebot
                {
                    Loesung[i, j] = ekm[i, ekm.GetLength(1) - 1];
                    Loesung[Loesung.GetLength(0) - 1, j] = ekm[ekm.GetLength(0) - 1, j] - ekm[i, ekm.GetLength(1) - 1]; //Neuen Bedarf ausrechnen
                    ekm[ekm.GetLength(0) - 1, j] = ekm[ekm.GetLength(0) - 1, j] - ekm[i, ekm.GetLength(1) - 1]; //Neuen Bedarf auch in ekm schreiben
                    ekm[i, ekm.GetLength(1) - 1] = 0;
                    Loesung[i, Loesung.GetLength(1) - 1] = 0;
                    i++;
                }
                else // ==
                {
                    Loesung[i, j] = ekm[ekm.GetLength(0) - 1, j];
                    ekm[ekm.GetLength(0) - 1, j] = 0;
                    ekm[i, ekm.GetLength(1) - 1] = 0;
                    Loesung[Loesung.GetLength(0) - 1, j] = 0;
                    Loesung[i, Loesung.GetLength(1) - 1] = 0;
                    i++;
                    j++;
                }
            }
            return Loesung;
        }
        /// <summary>
        /// Berechnet die Kosten eines gelösten Transportproblems anhand der Einheitskostenmatrix.
        /// </summary>
        /// <param name="Loesung">Gelöstes Transportproblem</param>
        /// <param name="ekm">Einheitskostenmatrix zum gelösten Transportproblem</param>
        /// <returns></returns>
        public int BerechneKosten(int[,] Loesung, int[,] ekm)
        {
            int k = 0;
            for (int i = 0; i < Loesung.GetLength(0); i++)
            {
                for (int j = 0; j < Loesung.GetLength(1); j++)
                {
                    if (Loesung[i, j] != 0)
                    {
                        k += Loesung[i, j] * ekm[i, j];
                    }
                }
            }
            return k;
        }
        /// <summary>
        /// Löst ein Transportproblem nach der Matrixminimum-Methode und gibt eine entsprechende Lösungsmatrix in Form eines zweidimensionalen integer-Arrays zurück. Die zurückgegebene Matrix hat logischerweise die gleichen Ausmaße wie die übergebene Matrix. Die Lösungsmatrix wird auch ausgegeben.
        /// </summary>
        /// <param name="ekm">Einheitskostenmatrix mit Angebots- und Bedarfsinformationen in einer Matrix kombiniert dargestellt.</param>
        /// <returns></returns>
        public int[,] MatrixminimumLoesung(int[,] ekm)
        {
            int[,] Loesung = new int[ekm.GetLength(0), ekm.GetLength(1)];

            Loesung = BedarfAngebotReinschreiben(ekm);
            //Lösungsmatrix der Übersichtlichkeit halber mit Angebot und Bedarfsinformationen versehen
            //for (int ii = 0; ii < Loesung.GetLength(1); ii++) //Bedarfszeile, so oft wie Spalten vorhanden sind, deshlb GetLength(1), 1=Spalten
            //{
            //    Loesung[Loesung.GetLength(0) - 1, ii] = ekm[ekm.GetLength(0) - 1, ii];
            //}
            //for (int jj = 0; jj < Loesung.GetLength(0); jj++)
            //{
            //    Loesung[jj, Loesung.GetLength(1) - 1] = ekm[jj, ekm.GetLength(1) - 1];
            //}

            int kleinsteKosten = int.MaxValue;
            int[,] zweiteEkm = new int[ekm.GetLength(0), ekm.GetLength(1)];
            Array.Copy(ekm, zweiteEkm, ekm.GetLength(0) * ekm.GetLength(1));
            int rememberi = 0;
            int rememberj = 0;

            int MaximaleSchleifendurchlaeufe = (ekm.GetLength(0) - 1) * (ekm.GetLength(1) - 1);
            int counter = 0;

            bool stop = false;
            bool ichWarHier = false;


            while (!stop && counter <= MaximaleSchleifendurchlaeufe)
            {
                for (int i = 0; i < zweiteEkm.GetLength(0) - 1; i++) //GetLength(0) = Zeilen
                {
                    if (Loesung[i, Loesung.GetLength(1) - 1] == 0)
                    {
                        //Zeile ignorieren wenn Angebot = 0 ist
                    }
                    else
                    {
                        for (int j = 0; j < zweiteEkm.GetLength(1) - 1; j++) //GetLength(1) = Spalten
                        {
                            if (Loesung[Loesung.GetLength(0) - 1, j] == 0)
                            {
                                //Spalte ignorieren, wenn Bedarf = 0 ist
                            }
                            else
                            {
                                //kleinsteKosten = kleinsteKosten > ekm[i, j] ? ekm[i, j] : kleinsteKosten ;
                                if (kleinsteKosten > zweiteEkm[i, j] && zweiteEkm[i, j] != int.MaxValue)
                                {
                                    kleinsteKosten = zweiteEkm[i, j];
                                    rememberi = i;
                                    rememberj = j;
                                    ichWarHier = true;
                                }
                            }
                        }
                    }
                }

                stop = ichWarHier ? false : true;
                ichWarHier = false;
                if (stop == false)
                {
                    if (Loesung[rememberi, Loesung.GetLength(1) - 1] != Loesung[Loesung.GetLength(0) - 1, rememberj])
                    {
                        if (Loesung[rememberi, Loesung.GetLength(1) - 1] > Loesung[Loesung.GetLength(0) - 1, rememberj])
                        {
                            Loesung[rememberi, rememberj] = Loesung[Loesung.GetLength(0) - 1, rememberj];
                            Loesung[rememberi, Loesung.GetLength(1) - 1] = Loesung[rememberi, Loesung.GetLength(1) - 1] - Loesung[Loesung.GetLength(0) - 1, rememberj];
                            Loesung[Loesung.GetLength(0) - 1, rememberj] = 0;
                            zweiteEkm[zweiteEkm.GetLength(0) - 1, rememberj] = 0;
                        }
                        else
                        {
                            Loesung[rememberi, rememberj] = Loesung[rememberi, Loesung.GetLength(1) - 1];
                            Loesung[Loesung.GetLength(0) - 1, rememberj] = Loesung[Loesung.GetLength(0) - 1, rememberj] - Loesung[rememberi, Loesung.GetLength(1) - 1];
                            Loesung[rememberi, Loesung.GetLength(1) - 1] = 0;
                            zweiteEkm[rememberi, zweiteEkm.GetLength(1) - 1] = 0;
                        }

                    }
                    else //Gleichheit von Bedarf und Angebot
                    {
                        Loesung[rememberi, rememberj] = Loesung[rememberi, Loesung.GetLength(1) - 1];
                        Loesung[rememberi, Loesung.GetLength(1) - 1] = 0;
                        Loesung[Loesung.GetLength(0) - 1, rememberj] = 0;
                        zweiteEkm[rememberi, zweiteEkm.GetLength(1) - 1] = 0;
                        zweiteEkm[zweiteEkm.GetLength(0) - 1, rememberj] = 0;

                    }


                    Console.WriteLine(counter + 1 + ". Schritt:");
                    MatrixAusgeben(Loesung);
                }
                kleinsteKosten = int.MaxValue;
                zweiteEkm[rememberi, rememberj] = int.MaxValue;
                counter++;

            }
            return Loesung;
        }
        /// <summary>
        /// Löst ein Transportproblem nach der Spaltenminimum-Methode und gibt eine entsprechende Lösungsmatrix in Form eines zweidimensionalen integer-Arrays zurück. Die zurückgegebene Matrix hat logischerweise die gleichen Ausmaße wie die übergebene Matrix. Die Lösungsmatrix wird auch ausgegeben.
        /// </summary>
        /// <param name="ekm">Einheitskostenmatrix mit Angebots- und Bedarfsinformationen in einer Matrix kombiniert dargestellt.</param>
        /// <returns></returns>
        public int[,] SpaltenminiumLoesung(int[,] ekm)
        {

            int[,] Loesung = new int[ekm.GetLength(0), ekm.GetLength(1)];

            Loesung = BedarfAngebotReinschreiben(ekm);
            //Lösungsmatrix der Übersichtlichkeit halber mit Angebot und Bedarfsinformationen versehen
            //for (int ii = 0; ii < Loesung.GetLength(1); ii++) //Bedarfszeile, so oft wie Spalten vorhanden sind, deshlb GetLength(1), 1=Spalten
            //{
            //    Loesung[Loesung.GetLength(0) - 1, ii] = ekm[ekm.GetLength(0) - 1, ii];
            //}
            //for (int jj = 0; jj < Loesung.GetLength(0); jj++)
            //{
            //    Loesung[jj, Loesung.GetLength(1) - 1] = ekm[jj, ekm.GetLength(1) - 1];
            //}

            int kleinsteKosten = int.MaxValue;
            int[,] zweiteEkm = new int[ekm.GetLength(0), ekm.GetLength(1)];
            Array.Copy(ekm, zweiteEkm, ekm.GetLength(0) * ekm.GetLength(1));
            int rememberi = 0;
            int rememberj = 0;

            int MaximaleSchleifendurchlaeufe = ekm.GetLength(1) - 1; //Anzahl Spalten ohne Angebotsspalte
            int counter = 0;

            bool stop = false;
            bool ichWarHier = false;


            while (!stop && counter <= MaximaleSchleifendurchlaeufe)
            {
                //Diese For-Schleife ist der einzige Unterschied zur Funktion MatrixminimumLoesung und ersetzt den unten auskommentierten Teil
                for (int i = 0; i < zweiteEkm.GetLength(0) - 1; i++)
                {
                    if (kleinsteKosten > zweiteEkm[i, counter] && zweiteEkm[i, counter] != int.MaxValue)
                    {
                        kleinsteKosten = zweiteEkm[i, counter];
                        rememberi = i;
                        rememberj = counter;
                        ichWarHier = true;
                    }
                }


                //Dieser Teil wählt das Element beim Matrixminimumverfahren aus
                //for (int i = 0; i < zweiteEkm.GetLength(0) - 1; i++) //GetLength(0) = Zeilen
                //{
                //    if (Loesung[i, Loesung.GetLength(1) - 1] == 0)
                //    {
                //        //Zeile ignorieren wenn Angebot = 0 ist
                //    }
                //    else
                //    {
                //        for (int j = 0; j < zweiteEkm.GetLength(1) - 1; j++) //GetLength(1) = Spalten
                //        {
                //            if (Loesung[Loesung.GetLength(0) - 1, j] == 0)
                //            {
                //                //Spalte ignorieren, wenn Bedarf = 0 ist
                //            }
                //            else
                //            {
                //                //kleinsteKosten = kleinsteKosten > ekm[i, j] ? ekm[i, j] : kleinsteKosten ;
                //                if (kleinsteKosten > zweiteEkm[i, j] && zweiteEkm[i, j] != int.MaxValue)
                //                {
                //                    kleinsteKosten = zweiteEkm[i, j];
                //                    rememberi = i;
                //                    rememberj = j;
                //                    ichWarHier = true;
                //                }
                //            }
                //        }
                //    }
                //}

                stop = ichWarHier ? false : true;
                ichWarHier = false;
                if (stop == false)
                {
                    if (Loesung[rememberi, Loesung.GetLength(1) - 1] != Loesung[Loesung.GetLength(0) - 1, rememberj])
                    {
                        if (Loesung[rememberi, Loesung.GetLength(1) - 1] > Loesung[Loesung.GetLength(0) - 1, rememberj])
                        {
                            Loesung[rememberi, rememberj] = Loesung[Loesung.GetLength(0) - 1, rememberj];
                            Loesung[rememberi, Loesung.GetLength(1) - 1] = Loesung[rememberi, Loesung.GetLength(1) - 1] - Loesung[Loesung.GetLength(0) - 1, rememberj];
                            Loesung[Loesung.GetLength(0) - 1, rememberj] = 0;
                            zweiteEkm[zweiteEkm.GetLength(0) - 1, rememberj] = 0;
                        }
                        else
                        {
                            Loesung[rememberi, rememberj] = Loesung[rememberi, Loesung.GetLength(1) - 1];
                            Loesung[Loesung.GetLength(0) - 1, rememberj] = Loesung[Loesung.GetLength(0) - 1, rememberj] - Loesung[rememberi, Loesung.GetLength(1) - 1];
                            Loesung[rememberi, Loesung.GetLength(1) - 1] = 0;
                            zweiteEkm[rememberi, zweiteEkm.GetLength(1) - 1] = 0;
                        }

                    }
                    else //Gleichheit von Bedarf und Angebot
                    {
                        Loesung[rememberi, rememberj] = Loesung[rememberi, Loesung.GetLength(1) - 1];
                        Loesung[rememberi, Loesung.GetLength(1) - 1] = 0;
                        Loesung[Loesung.GetLength(0) - 1, rememberj] = 0;
                        zweiteEkm[rememberi, zweiteEkm.GetLength(1) - 1] = 0;
                        zweiteEkm[zweiteEkm.GetLength(0) - 1, rememberj] = 0;

                    }


                    Console.WriteLine(counter + 1 + ". Schritt:");
                    MatrixAusgeben(Loesung);
                }
                kleinsteKosten = int.MaxValue;
                zweiteEkm[rememberi, rememberj] = int.MaxValue;
                counter++;

            }
            return Loesung;
        }
        /// <summary>
        /// Schreibt Bedarfe und Angebote aus einer kombiniert dargestellten Einheitskostenmatrix in eine andere, (nicht zwangsläufig) leere Matrix rein.
        /// </summary>
        /// <param name="ekm">Einheitskostenmatrix</param>
        /// <returns></returns>
        public static int[,] BedarfAngebotReinschreiben(int[,] ekm)
        {
            int[,] abc = new int[ekm.GetLength(0), ekm.GetLength(1)];
            //Lösungsmatrix der Übersichtlichkeit halber mit Angebot und Bedarfsinformationen versehen
            for (int ii = 0; ii < abc.GetLength(1); ii++) //Bedarfszeile, so oft wie Spalten vorhanden sind, deshlb GetLength(1), 1=Spalten
            {
                abc[abc.GetLength(0) - 1, ii] = ekm[ekm.GetLength(0) - 1, ii];
            }
            for (int jj = 0; jj < abc.GetLength(0); jj++)
            {
                abc[jj, abc.GetLength(1) - 1] = ekm[jj, ekm.GetLength(1) - 1];
            }
            return abc;
        }
        /// <summary>
        /// Nimmt eine durch Nordwesteckenregel o. ä. voroptimierte Matrix entegegen und gibt die Lösung nach einer Stepping Stone Iteration zurück.
        /// </summary>
        /// <param name="ekm">Einheitskostenmatrix</param>
        /// <param name="voroptimiert">Matrix, die bereits nach Nordwesteckenregel, Spalten- oder Matrixminimum-Verfahren voroptimiert wurde.</param>
        /// <returns></returns>
        public int[,] SteppingStoneIteration(int[,] ekm, int[,] voroptimiert)
        {
            //voroptimiert = BedarfAngebotReinschreiben(ekm);
            for (int ii = 0; ii < voroptimiert.GetLength(0); ii++)
            {
                for (int jj = 0; jj < voroptimiert.GetLength(1); jj++)
                {
                    if (voroptimiert[ii, jj] == 0)
                    {
                        voroptimiert[ii, jj] = -1; //-1 als Ersatz für null, da null nicht zugewiesen werden kann.
                    }
                }
            }

            int referenzi = -1; //referenzi & referenzj: Koordinaten der leeren Zelle, die neu berechnet werden soll
            int referenzj = -1;

            //Leere Zelle suchen
            for (int ii = 0; ii < voroptimiert.GetLength(0); ii++)
            {
                for (int jj = 0; jj < voroptimiert.GetLength(1); jj++)
                {
                    if (voroptimiert[ii, jj] == -1)
                    {
                        referenzi = ii;
                        referenzj = jj;
                        break;
                    }
                }
                if (referenzi != -1)
                {
                    break;
                }
            }

            int[] iArray = new int[voroptimiert.GetLength(0) * voroptimiert.GetLength(1)];
            int[] jArray = new int[voroptimiert.GetLength(0) * voroptimiert.GetLength(1)];

            int i = referenzi;
            int j = referenzj;
            bool zelleGefunden = false;
            bool pfadGefunden = false;

            //        int[] PathCoordinates = new int[2] { -1, -1 };
            int counter = 0;
            int countCoordinates = 0;

            int[][] PathCoordinates = new int[(voroptimiert.GetLength(0) - 1) * (voroptimiert.GetLength(1) - 1)][];
            for (int y = 0; y < PathCoordinates.GetLength(0); y++)
            {   //PathCoordinates[x][0] gibt Zeilenwert (i) für Nummer x an,
                //PathCoordinates[x][1] gibt Spaltenwert (j) für Nummer x an,
                //PathCoordinates[x][2] gibt an, wie oft bei LookInDirection nix gefunden wurde.
                //1. Fehlversuch x hochzählen, wenn keins vorhanden, zurückgehen und 

                PathCoordinates[y] = new int[3] { -1, -1, -2 };
            }

            int direction = 1;
            int x = 1;
            int usedLength;
            while (!pfadGefunden && direction < 5)
            {
                while (PathCoordinates[countCoordinates][0] == -1)
                {
                    usedLength = GiveUsedLength(PathCoordinates);
                    if (usedLength < 0)
                    { //nur wenn PathCoordinates nicht leer ist, soll der Pfad auf Vollständigkeit geprüft werden.
                        //Wenn das hier leergelassen wird, kommt die innere While-Schleife nicht über die erste Iteration hinaus, da CheckIfPathCompleted bei leerem PathCompleted fälschlicherweise true zurück gibt.
                        pfadGefunden = CheckIfPathCompleted(PathCoordinates);
                    }
                    if (pfadGefunden)
                    { //Wenn der Pfad gefunden ist, innere While-Schleife abbrechen, äußere bricht wegen pfadGefunden von selbst ab.
                        break;
                    }
                    PathCoordinates[countCoordinates] = LookInDirection(i, j, direction, x, voroptimiert);
                    i = PathCoordinates[countCoordinates][0];
                    j = PathCoordinates[countCoordinates][1];
                    countCoordinates++;
                    direction++;
                }
                PathCoordinates[countCoordinates] = LookInDirection(i, j, direction, x, voroptimiert);
                if (PathCoordinates[countCoordinates][0] == -1)
                {
                    x++;
                }
                else
                {
                    x = 1;
                    direction++;
                    i = PathCoordinates[countCoordinates][0];
                    j = PathCoordinates[countCoordinates][1];
                }
            }


            //auch schon alte lösug, ausversehen schon auf pathcoordinates[countcoordinates]... umgestellt.
            while (PathCoordinates[countCoordinates][0] == -1 && direction < 5)
            { //nach rechts, unten, links, oben suchen bis was gefunden wurde.
                PathCoordinates[countCoordinates] = LookInDirection(referenzi, referenzj, direction, 1, voroptimiert);
                i = PathCoordinates[countCoordinates][0];
                j = PathCoordinates[countCoordinates][1];
                direction++;
            }
            //nachdem oben 1. Pfad-Zelle gefunden wurde, nächste direction suchen.
            PathCoordinates[countCoordinates] = LookInDirection(i, j, direction, 1, voroptimiert);
            if (PathCoordinates[countCoordinates][0] == -1)
            {
                //n
            }





            //int[] iRechts = new int[voroptimiert.GetLength(1)];
            //int[] jRechts = new int[voroptimiert.GetLength(1)];
            //for (int x = 0; x < iRechts.GetLength(0); x++)
            //{ //iRechts und jRechts mit -1 initialisieren, um 0-Werte als 0 zu identifizieren und nicht als null
            //    iRechts[x] = -1;
            //}
            //for (int y = 0; y < jRechts.GetLength(0); y++)
            //{
            //    jRechts[y] = -1;
            //}

            //while (j < voroptimiert.GetLength(1) - 1)
            //{
            //    j++; //nach rechts gehen
            //    if (voroptimiert[i, j] != -1)
            //    {
            //        iRechts[counter] = i;
            //        jRechts[counter] = j;
            //        counter++;
            //    }
            //}
            //do
            //{
            //    i++;//nach unten gehen für alle mögl. felder


            //} while (1 == 1);

            //Pfad suchen
            do
            {
                j++; //Zelle nach rechts gehen
                if (voroptimiert[i, j] != -1 && !pfadGefunden) //Wenn leere Zelle gefunden dann
                {
                    iArray[counter] = i; //i und j speichern und
                    jArray[counter] = j;
                    counter++; // counter hochzählen um nächste Zelle speichern zu können.
                    //rechtsNixGefunden = false;
                    do //Nächste Zelle suchen.
                    {
                        i++; //Zelle nach unten gehen.
                        if (voroptimiert[i, j] != -1 && !pfadGefunden) //wenn leere Zelle gefunden, i und j speichern.
                        {
                            iArray[counter] = i;
                            jArray[counter] = j;
                            //gefunden = true;
                            counter++;

                            do
                            {
                                j--; //Zelle nach links gehen.
                                if (voroptimiert[i, j] != -1 && !pfadGefunden)
                                {
                                    iArray[counter] = i;
                                    jArray[counter] = j;
                                    counter++;
                                    do
                                    {
                                        i--; // Zelle nach oben gehen.
                                        if (i == referenzi && j == referenzj)
                                        {
                                            pfadGefunden = true;
                                        }
                                    } while (i >= 0);
                                }
                            } while (j >= 0);
                        }
                    } while (i < voroptimiert.GetLength(0) - 1);
                }
            } while (j < voroptimiert.GetLength(1) - 1);







            do
            {
                j--; //Zelle nach rechts gehen
                if (voroptimiert[i, j] != -1) //Wenn leere Zelle gefunden dann
                {
                    iArray[counter] = i; //i und j speichern und
                    jArray[counter] = j;
                    counter++; // counter hochzählen um nächste Zelle speichern zu können.
                    do //Nächste Zelle suchen.
                    {
                        i++; //Zelle nach unten gehen.
                        if (voroptimiert[i, j] != -1) //wenn leere Zelle gefunden, i und j speichern.
                        {
                            iArray[counter] = i;
                            jArray[counter] = j;
                            zelleGefunden = true;
                            counter++;
                        }
                    } while (i < voroptimiert.GetLength(0) - 1);
                    if (zelleGefunden) //(anders prüfen!) wenn keine Zelle gefunden, nicht nach unten gehen sondern
                    {
                        zelleGefunden = false;
                        do
                        {
                            i--; //nach oben gehen.
                            if (voroptimiert[i, j] != -1) //wenn leere Zelle gefunden, i und j speichern.
                            {
                                iArray[counter] = i;
                                jArray[counter] = j;
                                counter++;
                            }
                        } while (i >= 0);
                    }
                    //hier irgendwie prüfen, ob jarray[counter] nen wert hat und dann counter hochzählen
                }
            } while (j >= 0);

            return voroptimiert;
        }
        /// <summary>
        /// Gibt die Koordinaten der nächsten Zelle eines Stepping-Stone-Pfades für eine bestimmte Richtung zurück.
        /// </summary>
        /// <param name="referenzi">Zeilen-Koordinate, von wo aus gesucht werden soll.</param>
        /// <param name="referenzj">Spalten-Koordinate, von wo aus gesucht werden soll.</param>
        /// <param name="direction">Richtung, in die gesucht werden soll: 1 = rechts, 2 = unten, 3 = links, 4 = oben.</param>
        /// <param name="WieVieltes">1, wenn das erste gefundene zurück gegeben werden soll, 2 für das zweite gefundene etc.</param>
        /// <param name="voroptimiert">voroptimierte Matrix, in der gesucht werden soll.</param>
        /// <returns>eindimensionales int-Array, ergebnis[0] ist Zeilen-Koordinate, ergebnis[1] ist Spalten-Koordinate</returns>
        public static int[] LookInDirection(int referenzi, int referenzj, int direction, int WieVieltes, int[,] voroptimiert)
        {
            int i = referenzi;
            int j = referenzj;
            int counter = 1;
            int[] ergebnis = new int[2] { -1, -1 };
            switch (direction)
            {
                case 1: //nach rechts suchen
                    while (j < voroptimiert.GetLength(1) - 1 && counter <= WieVieltes)
                    {
                        if (voroptimiert[i, j] != -1)
                        {
                            ergebnis[0] = i;
                            ergebnis[1] = j;
                            counter++;
                        }
                        j++;
                    }
                    break;
                case 2: //nach unten suchen
                    while (i < voroptimiert.GetLength(0) - 1 && counter <= WieVieltes)
                    {
                        if (voroptimiert[i, j] != -1)
                        {
                            ergebnis[0] = i;
                            ergebnis[1] = j;
                            counter++;
                        }
                        i++;
                    }
                    break;
                case 3: //nach links suchen
                    while (j > 0 && counter <= WieVieltes)
                    {
                        if (voroptimiert[i, j] != -1)
                        {
                            ergebnis[0] = i;
                            ergebnis[1] = j;
                            counter++;
                        }
                        j--;
                    }
                    break;
                case 4: //nach oben suchen
                    while (i > 0 && counter <= WieVieltes)
                    {
                        if (voroptimiert[i, j] != -1)
                        {
                            ergebnis[0] = i;
                            ergebnis[1] = j;
                            counter++;
                        }
                        j--;
                    }
                    break;
                default:
                    Console.WriteLine("Fehler beim Suchen des Pfades für Stepping Stone. Variable direction darf nur zwischen 1 und 4 sein.");
                    Console.ReadLine();
                    Environment.Exit(1);
                    break;
            }

            return ergebnis;
        }
        /// <summary>
        /// Gibt true zurück, wenn die übergebene Variable PathCoordinates einen geschlossenen Kreis darstellt und der Stepping-Stone-Pfad somit gefunden ist.
        /// </summary>
        /// <param name="PathCoordinates">Array, das eindimensionale int-Arrays mit der Länge 2 oder 3 besitzt, welche die Koordinaten des Stepping-Stone-Pfades beinhalten.</param>
        /// <returns>true, wenn der Pfad einen geschlossenen Kreis darstellt; false, wenn der Pfad noch nicht vollendet ist.</returns>
        public static bool CheckIfPathCompleted(int[][] PathCoordinates)
        {
            int usedLength = GiveUsedLength(PathCoordinates);

            if (PathCoordinates[usedLength][0] == PathCoordinates[0][0] && PathCoordinates[usedLength][1] == PathCoordinates[0][1])
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Gibt zurück, wie viele Elemente des Arrays PathCoordinates tatsächlihc benutzt wurden, also aus wie vielen Koordinaten der Stepping-Stone-Pfad besteht.
        /// </summary>
        /// <param name="PathCoordinates">Array, das eindimensionale int-Arrays mit der Länge 2 oder 3 besitzt, welche die Koordinaten des Stepping-Stone-Pfades beinhalten.</param>
        /// <returns>Anzahl der Koordinaten des Stepping-Stone-Pfades, der in PathCoordinates gespeichert ist.</returns>
        public static int GiveUsedLength(int[][] PathCoordinates)
        {
            int usedLength = 0;
            for (int countCoordinates = 0; countCoordinates < PathCoordinates.Length; countCoordinates++)
            {
                if (PathCoordinates[countCoordinates][0] != -1)
                {
                    usedLength++;
                }
                else
                {
                    break;
                }
            }
            return usedLength;
        }
    }
}
